
```
CV_Builder
├─ .vscode
│  └─ settings.json
├─ backend
│  ├─ .env
│  ├─ .vscode
│  │  └─ settings.json
│  ├─ controller
│  │  ├─ ResumeController.js
│  │  └─ UserController.js
│  ├─ middleware
│  │  └─ requireAuth.js
│  ├─ models
│  │  ├─ ResumeModel.js
│  │  └─ UserModel.js
│  ├─ package-lock.json
│  ├─ package.json
│  ├─ routes
│  │  ├─ resume.js
│  │  └─ user.js
│  └─ server.js
├─ frontend
│  ├─ .eslintrc.cjs
│  ├─ .vscode
│  │  └─ settings.json
│  ├─ dist
│  │  ├─ assets
│  │  │  ├─ index-422a11be.js
│  │  │  └─ index-d0cf91db.css
│  │  ├─ index.html
│  │  └─ vite.svg
│  ├─ index.html
│  ├─ package-lock.json
│  ├─ package.json
│  ├─ postcss.config.js
│  ├─ public
│  │  └─ vite.svg
│  ├─ src
│  │  ├─ App.jsx
│  │  ├─ assets
│  │  │  ├─ alpine.js
│  │  │  ├─ images
│  │  │  │  ├─ template1.jpg
│  │  │  │  ├─ template2.jpg
│  │  │  │  └─ template3.jpg
│  │  │  └─ react.svg
│  │  ├─ components
│  │  │  ├─ DownloadTemplate1.jsx
│  │  │  ├─ DownloadTemplate2.jsx
│  │  │  ├─ DownloadTemplate3.jsx
│  │  │  ├─ Form.jsx
│  │  │  ├─ Home.jsx
│  │  │  ├─ List.jsx
│  │  │  ├─ Login.jsx
│  │  │  ├─ Navbar.jsx
│  │  │  ├─ Register.jsx
│  │  │  ├─ Template.jsx
│  │  │  ├─ Template1.jsx
│  │  │  ├─ Template2.jsx
│  │  │  └─ Template3.jsx
│  │  ├─ context
│  │  │  ├─ AuthContext.jsx
│  │  │  └─ ResumesContext.jsx
│  │  ├─ index.css
│  │  ├─ main.jsx
│  │  └─ routes
│  │     └─ AuthRoute.jsx
│  ├─ tailwind.config.js
│  └─ vite.config.js
├─ Images Of Project
│  ├─ Opera Snapshot_2023-06-08_205754_localhost.png
│  ├─ Opera Snapshot_2023-06-08_205841_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230132_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230150_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230238_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230252_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230304_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230328_localhost.png
│  ├─ Opera Snapshot_2023-06-08_230851_localhost.png
│  └─ Opera Snapshot_2023-06-08_231101_localhost.png
└─ vercel.json

```